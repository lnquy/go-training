package main

import (
"os"
"fmt"
)

type Controller interface {
	Open() error
	Set() error
	Close() error
}

type Led struct {
	name string
	file *os.File
}

func (l Led) Open() error {
	fmt.Printf("%v opened\n", l.name)
	return nil
}

func (l Led) Set() error {
	fmt.Printf("%v set\n", l.name)
	return nil
}

func (l Led) Close() error {
	fmt.Printf("%v closed\n", l.name)
	return nil
}

func NewLed(name string) Led {
	return Led{
		name: name,
		file: nil,
	}
}

func main() {
	var m map[string]Controller = make(map[string]Controller)

	m["A"] = NewLed("Led A")
	m["B"] = NewLed("Led B")
	m["C"] = NewLed("Led C")
	m["D"] = NewLed("Led D")

	fmt.Printf("Led map: %v\n", m)

	fmt.Printf("\nTraverse through led map:")
	for k, v := range m {
		fmt.Printf("\n%v - %v - Type of map's value: %T\n", k, v, v)
		tmp := v.(Led)
		fmt.Printf("   > Led name: %v\n", tmp.name)
		fmt.Printf("   > Led file: %v\n", tmp.file)
		tmp.Open()
		tmp.Set()
		tmp.Close()
	}
}

